module.exports = function(context) {
	let options = {
		length: 50
	};

	if (context.options && context.options.length && context.options[0]) {
		options.length = context.options[0];
	}

	return function(commit) {
		let firstLine = commit.firstLine();

		if (firstLine.length > options.length) {
			context.report({
				message: "Short description is too long.",
				loc: {
					line: 1,
					column: options.length
				}
			});
		}
	};
};

module.exports.schema = [
	{
		"type": "integer"
	}
];
