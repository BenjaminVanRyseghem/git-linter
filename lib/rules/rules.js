let rules = [
	"short-description-length",
	"short-description-pattern",
	"long-description-length",
	"long-description-pattern",
	"empty-line"
];

module.exports = {};

for (let rule of rules) {
	module.exports[rule] = require(`./${rule}/${rule}`);
}
