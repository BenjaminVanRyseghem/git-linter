module.exports = function(context) {
	return function(commit) {
		if (commit.numberOfLines() < 2) {
			return;
		}

		let secondLine = commit.secondLine();

		if (secondLine.length > 0) {
			context.report({
				message: "Second line should be empty.",
				loc: {
					line: 2,
					column: 1
				}
			});
		}
	};
};

module.exports.schema = [];
