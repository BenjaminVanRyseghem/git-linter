module.exports = function(context) {
	let options = {};

	if (context.options && context.options.length && context.options[0]) {
		options.pattern = context.options[0];
	}

	if (!options.pattern) {
		throw new Error("`pattern` parameter is missing");
	}

	return function(commit) {
		let firstLine = commit.firstLine();
		let pattern = new RegExp(options.pattern);

		if (!pattern.test(firstLine)) {
			context.report({
				message: `Short description is not matching "${options.pattern}"`,
				loc: {
					line: 1,
					column: 1
				}
			});
		}
	};
};

module.exports.schema = [
	{
		"type": "string"
	}
];
