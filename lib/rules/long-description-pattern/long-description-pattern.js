module.exports = function(context) {
	let options = {};

	if (context.options && context.options.length && context.options[0]) {
		options.pattern = context.options[0];
	}

	if (!options.pattern) {
		throw new Error("`pattern` parameter is missing");
	}

	return function(commit) {
		let {lineNumber, body} = commit.body();

		let pattern = new RegExp(options.pattern);
		let content = body.join("\n");

		if (!pattern.test(content)) {
			context.report({
				message: `Long description is not matching "${options.pattern}"`,
				loc: {
					line: lineNumber,
					column: 1
				}
			});
		}
	};
};

module.exports.schema = [
	{
		"type": "string"
	}
];
