module.exports = function(context) {
	let options = {
		length: 72
	};

	if (context.options && context.options.length && context.options[0]) {
		options.length = context.options[0];
	}

	return function(commit) {
		let {lineNumber, body} = commit.body();
		body.forEach((line, index) => {
			if (line.length > options.length) {
				context.report({
					message: "Long description is too long.",
					loc: {
						line: index + lineNumber,
						column: options.length
					}
				});
			}
		});
	};
};

module.exports.schema = [
	{
		"type": "integer"
	}
];
