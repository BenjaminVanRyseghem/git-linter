module.exports = ({
	path = "",
	mode = ""
}) => {
	let file = {};

	file.path = () => {
		return path;
	};

	file.mode = () => {
		return mode;
	};

	return file;
};
