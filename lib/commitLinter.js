module.exports = function({commit, ruleName, ruleFunction, ruleOptions, lintResult, extraContext}) {
	const context = require("./context");
	const isArray = require("./util/isArray");

	let severity;

	if (isArray(ruleOptions)) {
		ruleOptions = ruleOptions.slice();
		severity = ruleOptions.pop();
	} else {
		severity = ruleOptions;
		ruleOptions = null;
	}

	if (severity === 0) {
		return;
	}

	let data = Object.assign({
		level: severity,
		options: ruleOptions,
		lintResult: lintResult,
		rule: ruleName
	}, extraContext);

	let newContext = context(data);

	ruleFunction(newContext)(commit);
};
