module.exports = ({sha, files, message, type}) => {
	let commit = {};

	commit.sha = () => {
		return sha;
	};

	commit.shortSha = () => {
		return sha.slice(0.8);
	};

	commit.type = () => {
		return type;
	};

	commit.files = () => {
		return files;
	};

	commit.message = () => {
		return message;
	};

	commit.allLines = () => {
		return message.split("\n");
	};

	commit.lines = () => {
		return commit.allLines().filter((line) => {
			return line[0] !== "#";
		});
	};

	commit.comments = () => {
		return commit.allLines().filter((line) => {
			return line[0] === "#";
		});
	};

	commit.firstLine = () => {
		return commit.lines()[0];
	};

	commit.secondLine = () => {
		return commit.lines()[1];
	};

	commit.numberOfLines = () => {
		return commit.lines().length;
	};

	commit.body = () => {
		let lines = commit.lines();
		lines.shift(); // Drop the first line
		let start = 2;

		if (!lines[0].length) {
			lines.shift(); // Drop the optional empty line
			start = 3;
		}

		return {
			lineNumber: start,
			body: lines
		};
	};

	return commit;
};
