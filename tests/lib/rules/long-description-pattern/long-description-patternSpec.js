const rule = require("../../../../lib/rules/long-description-pattern/long-description-pattern");

describe("long description pattern", () => {
	it("does not report when the body is not matching the pattern (string)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = ["foo"];
		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: ["kkkfookkk"]});

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("reports when the body is not matching the pattern (string)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = ["foo"];
		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: ["kkkfokkk"]});

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});

	it("does not report when the body is not matching the pattern (regexp)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = [/^foo[k]+/i];

		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: ["FOOKKKK"]});

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("reports when the body is not matching the pattern (regexp)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = [/^foo[k]+/i];

		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: ["kkkfokkk"]});

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});

	it("throws an error if no pattern is provided", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let fn = rule.bind(null, context);

		expect(fn).toThrow(jasmine.stringMatching("pattern"));
		expect(fn).toThrow(jasmine.stringMatching("missing"));
	});
});
