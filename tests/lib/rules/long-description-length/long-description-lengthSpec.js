const rule = require("../../../../lib/rules/long-description-length/long-description-length");

describe("long description length", () => {
	it("reports when the second line is longer that 72 characters", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: ["foooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"]});

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});

	it("doesn't report when the second line is lesser that 72 characters", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: ["foooooo"]});

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("doesn't report when the body is empy", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: []});

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("use length argument when provided", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = ["4"];
		let commit = jasmine.createSpyObj("commit", ["body"]);
		commit.body.and.returnValue({body: ["foooooo"]});

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});
});
