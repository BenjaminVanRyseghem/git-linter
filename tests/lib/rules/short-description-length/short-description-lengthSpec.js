const rule = require("../../../../lib/rules/short-description-length/short-description-length");

describe("short description length", () => {
	it("reports when the first line is longer that 50 characters", () => {
		let context = jasmine.createSpyObj("context", ["report"]);

		let commit = jasmine.createSpyObj("commit", ["firstLine"]);
		commit.firstLine.and.returnValue("foooooooooooooooooooooooooooooooooooooooooooooooooo");

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});

	it("doesn't report when the first line is lesser that 50 characters", () => {
		let context = jasmine.createSpyObj("context", ["report"]);

		let commit = jasmine.createSpyObj("commit", ["firstLine"]);
		commit.firstLine.and.returnValue("foooo");

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("use length argument when provided", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = ["4"];

		let commit = jasmine.createSpyObj("commit", ["firstLine"]);
		commit.firstLine.and.returnValue("foooo");

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});
});
