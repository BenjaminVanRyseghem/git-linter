const rule = require("../../../../lib/rules/empty-line/empty-line");

describe("empty line", () => {
	it("reports when the second line is not empty", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let commit = jasmine.createSpyObj("commit", ["secondLine", "numberOfLines"]);
		commit.secondLine.and.returnValue("non empty string");

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});

	it("doesn't report when the second line is empty", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let commit = jasmine.createSpyObj("commit", ["secondLine", "numberOfLines"]);
		commit.secondLine.and.returnValue("");

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("doesn't report when there is only one line", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let commit = jasmine.createSpyObj("commit", ["secondLine", "numberOfLines"]);
		commit.numberOfLines.and.returnValue(1);

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});
});
