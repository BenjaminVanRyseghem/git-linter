const rule = require("../../../../lib/rules/short-description-pattern/short-description-pattern");

describe("short description pattern", () => {
	it("does not report when the first line is not matching the pattern (string)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = ["foo"];

		let commit = jasmine.createSpyObj("commit", ["firstLine"]);
		commit.firstLine.and.returnValue("kkkfookkk");

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("reports when the first line is not matching the pattern (string)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = ["foo"];

		let commit = jasmine.createSpyObj("commit", ["firstLine"]);
		commit.firstLine.and.returnValue("kkkfokkk");

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});

	it("does not report when the first line is not matching the pattern (regexp)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = [/^foo[k]+/i];

		let commit = jasmine.createSpyObj("commit", ["firstLine"]);
		commit.firstLine.and.returnValue("FOOKKKK");

		rule(context)(commit);

		expect(context.report).not.toHaveBeenCalled();
	});

	it("reports when the first line is not matching the pattern (regexp)", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		context.options = [/^foo[k]+/i];

		let commit = jasmine.createSpyObj("commit", ["firstLine"]);
		commit.firstLine.and.returnValue("kkkfokkk");

		rule(context)(commit);

		expect(context.report).toHaveBeenCalled();
	});

	it("throws an error if no pattern is provided", () => {
		let context = jasmine.createSpyObj("context", ["report"]);
		let fn = rule.bind(null, context);

		expect(fn).toThrow(jasmine.stringMatching("pattern"));
		expect(fn).toThrow(jasmine.stringMatching("missing"));
	});
});
