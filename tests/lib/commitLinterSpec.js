require("../helpers/spyOnModule");
const commitLinter = require("../../lib/commitLinter");

describe("commitLinter", () => {
	it("doesn't call ruleFunction when severity is `0`", function() {
		let ruleFunction = jasmine.createSpy("ruleFunction");
		let severity = 0;

		commitLinter({
			ruleOptions: severity,
			ruleFunction
		});

		expect(ruleFunction).not.toHaveBeenCalled();
	});

	it("uses last option as severity", function() {
		let ruleFunction = jasmine.createSpy("ruleFunction");
		ruleFunction.and.returnValue(() => {});
		let severity = 2;

		let commit = jasmine.createSpy("commit");
		let context = global.spyOnModule("../../lib/context");

		commitLinter({
			commit,
			ruleOptions: ["", severity],
			ruleFunction
		});

		expect(context.calls.argsFor(0)[0].level).toBe(severity);
	});

	it("flattens `extraContext` in the data", function() {
		let ruleFunction = jasmine.createSpy("ruleFunction");
		ruleFunction.and.returnValue(() => {});
		let severity = 2;
		let commit = jasmine.createSpy("commit");
		let extra = jasmine.createSpy("extra");
		let extraContext = {extra};

		let context = global.spyOnModule("../../lib/context");

		commitLinter({
			commit,
			ruleOptions: ["", severity],
			ruleFunction,
			extraContext
		});

		expect(context.calls.argsFor(0)[0].extra).toBe(extra);
	});

	it("calls the rules with actual commit", function() {
		let rule = jasmine.createSpy("rule");
		let ruleFunction = jasmine.createSpy("ruleFunction");
		ruleFunction.and.returnValue(rule);

		let severity = 2;

		let commit = jasmine.createSpy("commit");
		commitLinter({
			ruleOptions: severity,
			ruleFunction,
			commit
		});

		expect(rule).toHaveBeenCalledWith(commit);
	});
});
