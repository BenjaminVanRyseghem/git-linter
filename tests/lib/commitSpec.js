let commit = require("../../lib/commit");

describe("commit", () => {
	it("returns provided sha", function() {
		let sha = jasmine.createSpy("sha");

		let instance = commit({sha});

		expect(instance.sha()).toBe(sha);
	});

	it("returns provided files", function() {
		let files = jasmine.createSpy("files");

		let instance = commit({files});

		expect(instance.files()).toBe(files);
	});

	it("returns provided message", function() {
		let message = jasmine.createSpy("message");

		let instance = commit({message});

		expect(instance.message()).toBe(message);
	});

	it("returns all lines as array", () => {
		let instance = commit({message: "foo\nbar\nbaz"});

		expect(instance.allLines()).toEqual(["foo", "bar", "baz"]);
	});

	it("filters out comments", () => {
		let instance = commit({});
		spyOn(instance, "allLines").and.returnValue(["foo", "# bar", "baz"]);

		expect(instance.lines()).toEqual(["foo", "baz"]);
	});

	it("collects out comments", () => {
		let instance = commit({});
		spyOn(instance, "allLines").and.returnValue(["# foo", "bar", "# baz"]);

		expect(instance.comments()).toEqual(["# foo", "# baz"]);
	});

	it("returns the first line of all useful lines", () => {
		let instance = commit({});
		let firstLine = jasmine.createSpy("firstLine");
		spyOn(instance, "lines").and.returnValue([firstLine]);

		expect(instance.firstLine()).toBe(firstLine);
	});

	it("returns the second line of all useful lines", () => {
		let instance = commit({});
		let secondLine = jasmine.createSpy("secondLine");
		spyOn(instance, "lines").and.returnValue(["", secondLine]);

		expect(instance.secondLine()).toBe(secondLine);
	});

	it("returns the number of all useful lines", () => {
		let instance = commit({});
		let lines = ["", "", ""];
		let numberOfLines = lines.length;
		spyOn(instance, "lines").and.returnValue(lines);

		expect(instance.numberOfLines()).toBe(numberOfLines);
	});

	it("removes the first line from body", () => {
		let instance = commit({});
		spyOn(instance, "lines").and.returnValue(["foo", "bar", "baz"]);

		let {lineNumber, body} = instance.body();
		expect(lineNumber).toBe(2);
		expect(body).toEqual(["bar", "baz"]);
	});

	it("removes the second line form body when empty", () => {
		let instance = commit({});
		spyOn(instance, "lines").and.returnValue(["foo", "", "baz"]);

		let {lineNumber, body} = instance.body();
		expect(lineNumber).toBe(3);
		expect(body).toEqual(["baz"]);
	});
});
